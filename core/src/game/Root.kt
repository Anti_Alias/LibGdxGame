package game
import com.badlogic.ashley.core.Component
import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.*
import engine.core.Game
import game.generators.AreaLoaders


/**
 * LibGdx application root.
 * Initializes Game, runs it and tears it down when the application ends.
 * Represents application Main.
 */
class Root : ApplicationAdapter()
{
	/**
	 * Creates resources for application (Game, Asset manager, etc)
	 */
	override fun create()
	{
        // Initializes game
		val windowWidth:Int = Gdx.graphics.width
		val windowHeight:Int= Gdx.graphics.height
		Game.init(windowWidth, windowHeight)

        // Creates and transitions to first area.
        val area = AreaLoaders.load("secondmap", mapOf())
        Game.transitionTo(area)

        // Makes first Entity
        val components:List<Component> = listOf()
        val entity = Entity()
        components.forEach{entity.add(it)}

        // Adds it to the first area.
        area.engine.addEntity(entity)

	}

	/**
	 * Game loop. Runs Game.
	 */
	override fun render()
    {
		Game.run()
		Game.render()
	}

    /**
     * Handles window resizing.
     */
    override fun resize(width:Int, height:Int):Unit
    {

    }

	/**
	 * Destructor. Cleans up resources.
	 */
	override fun dispose()
	{
		Game.dispose()
	}
}