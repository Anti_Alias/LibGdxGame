package game.generators
import com.badlogic.ashley.core.Component
import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.TextureRegion
import engine.components.*
import engine.core.Game
import engine.math.Rect2
import engine.graphics.Sprite
import game.components.platformer.Body
import game.components.platformer.Cloud
import game.components.platformer.Jumper

/**
 * Alias for Entity generator.
 * The list represents a list of parseable parameters that can be used to
 * generate a type of Entity.
 */
typealias EntityGenerator = (Map<String, String>)->Entity


/**g
 * @author William Andrew Cahill
 * Represents globally accessible mapping of Entity types, parameters to functions that generate Entities.
 */
object EntityGenerators
{
    /**
     * Generates a Player given a list of String parameters.
     */
    val PLAYER: EntityGenerator =
    {
        // Constructs and returns Entity
        val entity = Entity()
        entity
    }

    /**
     * Generates a Player given a list of String parameters.
     */
    val DUMMY: EntityGenerator =
    {
        // Gets parameters
        val params:Map<String, String> = it

        // Gets ball graphic
        val region:TextureRegion = loadRegion("images/ball.png")
        val sprite = Sprite(region)

        // Constructs Body Component
        val rect = Rect2(0.0, 0.0, region.regionWidth.toDouble(), region.regionHeight.toDouble())
        val body = Body(rect)
        body.graphics = sprite
        body.weight = 1.0

        // Constructs Jumper Component
        val jumper = Jumper()
        jumper.maxSpeed = 10.0
        jumper.jumpPower = 10.0

        // Constructs Entity using Components specified.
        val components:List<Component> = listOf(body, jumper, PlayerTag())
        val entity = Entity()
        components.forEach{entity.add(it)}

        // Returns constructed Entity
        entity
    }

    /**
     * Represents Cloud generator
     */
    val CLOUD: EntityGenerator =
    {
        // Gets parameters
        val params:Map<String, String> = it
        val speed:Double = params.getOrDefault("speed", "1.0").toDouble()

        // Loads resources
        val region:TextureRegion = loadRegion("images/cloud.png")
        val sprite = Sprite(region)
        sprite.color = Color(1.0f, 1.0f, 1.0f, 0.3f)

        // Creates body
        val rect = Rect2(0.0, 0.0, region.regionWidth.toDouble(), region.regionHeight.toDouble())
        val body = Body(rect)
        body.graphics = sprite
        body.weight = 0.0
        body.isSolid = false

        // Constructs Entity
        val entity = Entity()
        entity.add(body)
        entity.add(Cloud(speed))

        // Returns constructed Entity
        entity
    }

    /**
     * Takes some path to a resource and returns its TextureRegion.
     * This method is synchronized.
     */
    private fun loadRegion(resource:String):TextureRegion
    {
        val assets = Game.assets
        assets.load(resource, Texture::class.java)
        assets.finishLoading()
        val tex:Texture = assets.get(resource)
        return TextureRegion(tex)
    }

    /**
     * Mapping of generator names to generators.
     */
    private val generators:Map<String, EntityGenerator> = mapOf(
        "player" to PLAYER,
        "dummy" to DUMMY,
        "cloud" to CLOUD
    )

    /**
     * Generates an Entity of a certain type with given parameters.
     *
     * @param name Name of generator to use.
     * @param params String parameters to pass into the generator.
     */
    fun generate(name:String, params:Map<String, String>):Entity
    {
        val generator: EntityGenerator = generators[name]!!
        return generator(params)
    }
}