package game.generators
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import engine.core.PlatformerArea
import engine.core.Game
import game.systems.platformer.BodyInit
import game.systems.platformer.BodyMovement
import game.systems.platformer.BodySync
import game.systems.platformer.CloudMovement
import game.systems.platformer.JumperKeyboardSystem


/**
 * Alias for lambda that takes arbitrary string arguments and
 * applies them to an PlatformerArea.
 */
typealias AreaLoader = (Map<String, String>, PlatformerArea) -> Unit

/**
 * @author William Andrew Cahill
 *
 * Singleton object responsible for loading Areas from a file.
 * Executable code can be added to the loading process is specified.
 */

object AreaLoaders
{
    /**
     * All loaders that can be applied to an PlatformerArea.
     * Use this to hook loaders for particular Areas.
     */
    var loaders:Map<String, AreaLoader> = mapOf()

    /**
     * Loads an PlatformerArea from a File with arbitrary parameters.
     *
     * @param name Name of the area to load.
     * This is typically a map's filename with no parent directory
     * and no file extension.  For instance, 'testarea' would evaluate to
     * 'maps/testarea.tmx
     *
     * @param params Arbitrary parameters to use when creating the PlatformerArea.
     * Typically, these parameters are specified in another .tmx file when
     * linking the map to another.
     */
    fun load(name:String, params:Map<String, String>): PlatformerArea
    {
        // Loads PlatformerArea from TiledMap
        val area = PlatformerArea.loadFromTiledMap(name)

        // Adds common Systems to the PlatformerArea.
        val systems = listOf(BodyInit(), BodyMovement(), BodySync(), JumperKeyboardSystem(), CloudMovement())
        systems.forEach{area.engine.addSystem(it)}

        // Creates label
        val manager: AssetManager = Game.assets
        val resource = "ui/basic-skin.json"
        manager.load(resource, Skin::class.java)
        manager.finishLoading()
        val skin:Skin = manager.get(resource)
        val label = Label("Pause  ", skin)


        // Adds UI
        area.addOnPause {
            val cx:Float = Game.initWidth/2f
            val cy:Float = Game.initHeight/2f
            label.setPosition(cx-label.width/2, cy-label.height/2)
            area.ui.addActor(label)
        }

        // Removes UI
        area.addOnUnpause {
            area.ui.removeActor(label)
        }

        // Runs code every frame
        area.addOnTick {
            
        }

        // Applies associated loader.
        val loader: AreaLoader = loaders.getOrElse(name, { LOADER_DEFAULT })
        loader.invoke(params, area)

        // Returns PlatformerArea created.
        return area
    }

    // ------------------------------- Actual Generators ------------------------------
    /**
     * Default generator
     */
    val LOADER_DEFAULT: AreaLoader = { params:Map<String, String>, dest: PlatformerArea ->

    }
}
