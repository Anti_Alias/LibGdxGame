package game.components.platformer
import com.badlogic.ashley.core.Component
import com.badlogic.gdx.scenes.scene2d.Actor
import engine.core.Surface
import engine.graphics.EmptyGraphics
import engine.math.Point2
import engine.math.Rect2
import engine.math.Vec2


/**
 * @author William Andrew Cahill
 * Represents an Entity's "body". A Body has a rect, a previous rect and a weight.
 *
 * @param rect Rectangle representing the location of the body as well as its size.
 */
class Body(var rect: Rect2) : Component
{
    /**
     * Graphics of this Body.
     * Default is empty.
     */
    var graphics: Actor = EmptyGraphics()

    /**
     * Desired layer of this Body.
     * Changing determines what layer to be
     * added to when parent Entity is added
     * to an Ashley engine.
     */
    internal var desiredLayer:Int = 0

    /**
     * Solid flag that determines if walls stop this Body.
     */
    var isSolid:Boolean = true

    /**
     * Previous rectangular state. Defaults to rect set initially in constructor.
     */
    var prevRect: Rect2 = rect

    /**
     * Velocity of the Body.
     */
    var vel: Vec2 = Vec2.ZERO

    /**
     * Weight of the Body. Determines how fast it falls.
     * "I know that's not how gravity works :)"
     */
    var weight:Double = 1.0

    /**
     * Friction of Body. Determines resistance
     * of body when in certain areas.
     *
     * Over the course of a single game tick, the environment should multiply this
     * value by ratios depending on the Body's situation.
     *
     * This value should be reset to 1 every frame by BodyInit System.
     */
    var friction:Double = 0.95

    /**
     * Current frictional value to use depending on if
     * the Body is on a surface or not.
     */
    val currentFriction:Double get()
    {
        val temp = prevSurface
        return if (temp != null) temp.friction else friction
    }

    /**
     * Previous surface this body was on, if any,.
     */
    var prevSurface: Surface? = null

    /**
     * Current surface this Body is on, if any,
     * Null if not.
     */
    var surface: Surface? = null

    /**
     * On ground flag.
     */
    val onGround:Boolean get() = surface != null

    /**
     * Previous onGround state.
     */
    val prevOnGround:Boolean get() = prevSurface != null

    /**
     * Point2 at the center of this Body.
     */
    val centerPoint: Point2 get() = object : Point2
    {
        override val x:Double get() = rect.x + rect.width/2
        override val y:Double get() = rect.y + rect.height/2
    }

    /**
     * Moves this Body based on delta Vec2 given.
     */
    fun move(delta: Vec2):Unit
    {
        rect = Rect2(rect.x+delta.x, rect.y+delta.y, rect.width, rect.height)
    }

    /**
     * Forces left side to be located at new x value.
     */
    fun forceLeft(newX:Double):Unit
    {
        rect = rect.copy(x=newX)
    }

    /**
     * Forces right side to be located at new x value.
     */
    fun forceRight(newX:Double):Unit
    {
        rect = rect.copy(x=newX-rect.width)
    }

    /**
     * Forces top side to be located at new y value.
     */
    fun forceTop(newY:Double):Unit
    {
        rect = rect.copy(y=newY-rect.height)
    }

    /**
     * Forces bottom side to be located at new y value.
     */
    fun forceBottom(newY:Double):Unit
    {
        rect = rect.copy(y=newY)
    }
}