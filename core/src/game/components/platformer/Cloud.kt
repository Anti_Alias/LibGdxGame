package game.components.platformer
import com.badlogic.ashley.core.Component

/**
 * Tag for clouds.
 *
 * @author William Andrew Cahill
 * @param scale Assumed scale of layer this cloud is on.
 */
class Cloud(val scale:Double) : Component
