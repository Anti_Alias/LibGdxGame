package game.components.platformer
import com.badlogic.ashley.core.Component
import engine.math.Vec2


/**
 * @author William Andrew Cahill
 *
 * Represents some jumping object.
 * Has the ability to jump around and be controlled.
 * Depends on Body component.
 *
 * All pubilc methods in this class are for convenience.
 * They should be invoked by their respective Systems.
 */
class Jumper : Component
{
    /**
     * Maximum horizontal speed of Jumper.
     */
    var maxSpeed:Double = 7.0
        get() = field
        set(value) = require(value >= 0.0){"Invalid maxSpeed $value. Must be >= 0.0"}

    /**
     * Power of jump
     */
    var jumpPower:Double = 15.0
        get() = field
        set(value) = require(value >= 0){"Invalid jump power $jumpPower. Must be >= 0.0"}

    /**
     * Has this Jumper move left.
     * Should not go faster than internal maxSpeed value.
     *
     * @param body Body to move left.
     * @param friction Assumed friction of environment.
     * Defaults to 0.95
     */
    fun moveLeft(body: Body, friction:Double = body.currentFriction):Unit
    {
        val rate:Double = calculateRate(maxSpeed, friction)
        body.vel -= Vec2(rate, 0.0)
    }

    /**
     * Has this Jumper move right.
     * Should not go faster than internal maxSpeed value.
     *
     * @param body Body to move right.
     * @param friction Assumed friction of environment.
     * Defaults to 0.95
     */
    fun moveRight(body: Body, friction:Double = body.currentFriction):Unit
    {
        val rate:Double = calculateRate(maxSpeed, friction)
        body.vel += Vec2(rate, 0.0)
    }

    /**
     * Causes the Jumper to jump in the air with the power
     * of jumpPower by default.
     */
    fun jump(body: Body, power:Double = jumpPower):Unit
    {
        val vx:Double = body.vel.x
        val vy:Double = jumpPower
        body.vel = Vec2(vx, vy)
    }


    /**
     * Helpful companion methods.
     */
    companion object
    {
        /**
         * Calculates the rate at which a Jumper should move
         * when considering maxSpeed and frictional values.
         */
        private fun calculateRate(maxSpeed:Double, friction:Double):Double
        {
            return maxSpeed * (1/friction - 1)
        }
    }
}