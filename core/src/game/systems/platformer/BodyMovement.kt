package game.systems.platformer
import com.badlogic.ashley.core.*
import engine.core.Priorities
import engine.math.Vec2
import engine.systems.CachedSystem
import game.components.platformer.Body


/**
 * @author William Andrew Cahill
 * Represents a system that moves Bodies. Updates their velocities based on gravity value as well as Weight.
 *
 * @param up Up vector which determines which way is up.
 * @param gravity Value that determines the falling speed of the BodyMovement.
 * @param airFriction Friction that the air applies to the Body.
 */
class BodyMovement(
    var gravity:Double = 0.5,
    var airFriction:Double = 0.99) : CachedSystem(family, Priorities.EXECUTE)
{
    // Companion object section
    companion object
    {
        /**
         * Family for class
         */
        val family = Family.all(Body::class.java).get()
    }

    /**
     * Updates Gravity code
     */
    override fun update(deltaTime:Float): Unit
    {
        for(entity in cachedEntities)
        {
            // Gets Body Component
            val body = entity.getComponent(Body::class.java)

            // Falls
            val vel = body.vel
            body.vel = (vel - Vec2(0.0, gravity*body.weight)).copy(x=vel.x*body.currentFriction)

            // Updates body position based on its velocity.
            body.move(body.vel)
        }
    }
}