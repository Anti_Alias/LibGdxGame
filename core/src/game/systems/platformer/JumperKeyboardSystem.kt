package game.systems.platformer
import com.badlogic.ashley.core.Family
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import game.components.platformer.Body
import game.components.platformer.Jumper
import engine.components.PlayerTag
import engine.core.*
import engine.systems.CachedSystem

/**
 * @author William Andrew Cahill
 *
 * Represents a System that controls Jumper Components with a Player Component tag.
 */
class JumperKeyboardSystem : CachedSystem(family, Priorities.DECIDE)
{

    // Companion object section
    companion object
    {
        /**
         * Family for class for Jumpers in question.
         */
        val family = Family.all(
            Jumper::class.java,
            Body::class.java,
            PlayerTag::class.java
        ).get()
    }

    /**
     * Updates JumperKeyboardSystem
     */
    override fun update(deltaTime:Float): Unit
    {
        // For all Entities that satisfy this criteria...
        for(entity in cachedEntities)
        {
            // Gets necessary Components
            val jumper: Jumper = Mappers.jumperMapper.get(entity)
            val body: Body = Mappers.bodyMapper.get(entity)

            // Functions based on keyboard input.
            if(Gdx.input.isKeyPressed(Input.Keys.LEFT))
            {
                jumper.moveLeft(body)
            }
            if(Gdx.input.isKeyPressed(Input.Keys.RIGHT))
            {
                jumper.moveRight(body)
            }
            if(body.prevOnGround && Gdx.input.isKeyPressed(Input.Keys.UP))
            {
                jumper.jump(body)
            }
        }
    }
}