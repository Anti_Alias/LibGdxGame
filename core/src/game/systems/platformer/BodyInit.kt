package game.systems.platformer
import com.badlogic.ashley.core.*
import engine.core.Priorities
import engine.systems.CachedSystem
import game.components.platformer.Body


/**
 * @author William Andrew Cahill
 * Represents a system that captures the state of Bodies at the start of a game loop before they change.
 */
class BodyInit : CachedSystem(family, Priorities.INIT)
{
    // Companion object section
    companion object
    {
        /**
         * Family for class
         */
        val family = Family.all(Body::class.java).get()
    }

    /**
     * Updates Gravity code
     */
    override fun update(deltaTime:Float): Unit
    {
        // Captures size and position of all Bodies
        for(entity in cachedEntities)
        {
            val body: Body = entity.getComponent(Body::class.java)
            body.prevRect = body.rect
            body.prevSurface = body.surface
            body.surface = null
        }
    }
}