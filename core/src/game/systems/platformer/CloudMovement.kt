package game.systems.platformer
import com.badlogic.ashley.core.*
import engine.core.*
import engine.math.*
import engine.systems.CachedSystem
import game.components.platformer.Body
import game.components.platformer.Cloud


/**
 * @author William Andrew Cahill
 * Represents a system that controls clouds.
 *
 */
class CloudMovement : CachedSystem(family, Priorities.EXECUTE)
{
    // Companion object section
    companion object
    {
        val family = Family.all(
            Cloud::class.java,
            Body::class.java
        ).get()
    }

    /**
     * Updates Gravity code
     */
    override fun update(deltaTime:Float): Unit
    {
        for(entity in cachedEntities)
        {
            // Gets Components
            val body: Body = Mappers.bodyMapper.get(entity)
            val cloud: Cloud = Mappers.cloudMapper.get(entity)

            // Handles components
            body.vel = Vec2(-1.0, 0.0)
            val bound: Rect2? = Game.currentArea?.camera?.boundary
            val left:Double = if(bound == null) 0.0 else bound.left
            if(body.graphics.x < left)
            {
                val area: Area = Game.currentArea!!
                val cam:Camera = area.camera
                val bound: Rect2? = cam.boundary
                val xCoord:Double = if(bound != null) bound.right * cloud.scale  else 2000.0
                body.forceLeft(body.rect.left + xCoord)
            }
        }
    }
}