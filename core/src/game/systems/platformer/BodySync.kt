package game.systems.platformer
import com.badlogic.ashley.core.*
import engine.core.Priorities
import engine.math.Vec2
import engine.systems.CachedSystem
import game.components.platformer.Body


/**
 * @author William Andrew Cahill
 *
 * Syncs the graphics of a Body object with its logical location.
 */
class BodySync(var gravity: Vec2 = Vec2.DOWN) : CachedSystem(BodyMovement.family, Priorities.SYNC)
{
    // Companion object section
    companion object
    {
        /**
         * Family for class
         */
        val family = Family.all(Body::class.java).get()
    }

    /**
     * Updates Gravity code
     */
    override fun update(deltaTime:Float): Unit
    {
        for(entity in cachedEntities)
        {
            // Gets position of body
            val body = entity.getComponent(Body::class.java)
            val pos: Vec2 = body.rect.bottomLeft

            // Sets body's graphical position to the bottom-left corner of the body's rect.
            // Also "rounds" coordinates
            body.graphics.setPosition(pos.x.toInt().toFloat(), pos.y.toInt().toFloat())
        }
    }
}