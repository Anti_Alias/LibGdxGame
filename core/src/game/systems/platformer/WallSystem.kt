package game.systems.platformer
import com.badlogic.ashley.core.ComponentMapper
import com.badlogic.ashley.core.Family
import game.components.platformer.Body
import engine.core.Direction
import engine.core.Priorities
import engine.walls.platformer.Wall
import engine.math.Rect2
import engine.math.EMath
import engine.events.platformer.CollisionEvent
import engine.systems.CachedSystem

/**
 * Represents a System that enforces collision from a TiledLayer.
 *
 * @author William Andrew Cahill
 * @param walls Backing array for all walls.
 * @param width Width of Walls array.
 * @param height Height of Walls array.
 * @param wallWidth Width of each wall.
 * @param wallheight Height of each wall.
 */
class WallSystem(
        val walls:Array<Wall?>,
        val width:Int,
        val height:Int,
        val wallWidth:Double,
        val wallHeight:Double
) : CachedSystem(family, Priorities.COLLISION_MAP)
{
    /**
     * Gets Wall at specified coordinates.
     */
    fun getWall(x:Int, y:Int): Wall? = walls[y*width+x]

    /**
     * Sets Wall at specified coordinates.
     */
    fun setWall(x:Int, y:Int, wall: Wall):Unit {walls[y*width+x] = wall}


    /**
     * Handles collision hooking.
     */
    private fun hook(x:Int, y:Int, lastX:Int, lastY:Int, body: Body):Boolean
    {
        // Checks if in bounds
        if(x >= 0 && x < width && y >= 0 && y < height)
        {
            // Gets wall at coordinates
            val wall: Wall? = getWall(x, y)
            if(wall != null)
            {
                val lastWall: Wall = getWall(lastX, lastY)!!
                val rect = Rect2(x * wallWidth, y * wallHeight, wallWidth, wallHeight)
                val lastRect = Rect2(lastX * wallWidth, lastY * wallHeight, wallWidth, wallHeight)

                // If have near-same y intercept...
                val coordX: Double = if (lastX < x) x * wallWidth else lastX * wallWidth
                val y0: Double = wall.surfaceY(rect, coordX)
                val y1: Double = lastWall.surfaceY(lastRect, coordX)
                if(Math.abs(y0 - y1) < 0.1)
                {
                    wall.hook(rect, body)
                }
            }
        }
        return false
    }

    /**
     * Enforces collision on body with wall at coordinates given.
     */
    private fun collide(x:Int, y:Int, body: Body):Unit
    {
        val wall: Wall? = getWall(x, y)
        if (wall != null)
        {
            val wallRect = Rect2(x * wallWidth, y * wallHeight, wallWidth, wallHeight)
            val event: CollisionEvent? = wall.collide(wallRect, body)
            if (event != null)
            {
                val exitSide: Direction? = event.exitSide
                if (exitSide == Direction.LEFT)
                {
                    // Attempts to enforce collision on left neighbors
                    if (!hook(x - +1, y + 1, x, y, body))
                        if (!hook(x - 1, y - 1, x, y, body))
                            hook(x - 1, y, x, y, body)
                }
                else if (exitSide == Direction.RIGHT)
                {
                    // Attempts to enforce collision on right neighbors
                    if(!hook(x+1, y+1, x, y, body))
                        if(!hook(x+1, y-1, x, y, body))
                            hook(x+1, y, x, y, body)

                }
            }
        }
    }

    /**
     * Updates the WallSystem
     */
    override fun update(deltaTime:Float):Unit
    {
        // For all Entities with bodies...
        for(entity in cachedEntities)
        {
            // Gets body in question with its current size
            val body: Body = bodyMapper.get(entity)

            // If body is solid...
            if(body.isSolid)
            {
                val rect: Rect2 = body.rect
                val prevRect: Rect2 = body.prevRect

                // Determines area that needs to be searched in pixels.
                val maxX: Double = width - 1.0
                val maxY: Double = height - 1.0
                val leftmost: Double = Math.min(rect.left, prevRect.left)
                val rightmost: Double = Math.max(rect.right, prevRect.right)
                val bottommost: Double = Math.min(rect.bottom, prevRect.bottom)
                val topmost: Double = Math.max(rect.top, prevRect.top)

                // Converts that area with respect to wall sizes and map limits
                val rectLeft: Int = EMath.clamp(leftmost / wallWidth, 0.0, maxX).toInt()
                val rectRight: Int = EMath.clamp(rightmost / wallWidth, 0.0, maxX).toInt()
                val rectBottom: Int = EMath.clamp(bottommost / wallHeight, 0.0, maxY).toInt()
                val rectTop: Int = EMath.clamp(topmost / wallHeight, 0.0, maxY).toInt()

                // Enforces collision on Walls that are withinb the bounds of the Body.
                for (y in rectBottom..rectTop)
                {
                    for (x in rectLeft..rectRight)
                    {
                        collide(x, y, body)
                    }
                }
            }
        }
    }


    /**
     * Helpful companion object to WallSystem class.
     */
    companion object
    {
        // Family of bodies.
        val family:Family = Family.all(Body::class.java).get()

        // Mapper for bodies
        val bodyMapper: ComponentMapper<Body> = ComponentMapper.getFor(Body::class.java)

        /**
         * Constructs a WallSystem based on a TiledMapTileLayer.
         */
        //fun fromTiledMapTileLayer(layer:TiledMapTileLayer): WallSystem
        //{

        //}
    }
}