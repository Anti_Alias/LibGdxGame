package engine.walls.platformer
import game.components.platformer.Body
import engine.core.Surface
import engine.events.platformer.CollisionEvent
import engine.math.Rect2


/**
 * Represents the data of a Wall object. Does not have concrete coordinates,
 * and relies on collision method passing in the position of this Wall to
 * determine how a particular collision is performed.
 *
 * @param p1 First Vec2
 * @param p2 Second Vec2
 */
interface Wall
{
    /**
     * Surface of the wall.
     */
    val surface:Surface

    /**
     * Function that calculates the Y value
     * of this Wall's surface given this Wall's pos
     * and some value x.
     * Behavior is undefined if hasSurface is false.
     *
     * @param x X coordinate being passed into the function
     * to generate a corresponding y value.
     */
    fun surfaceY(thisRect: Rect2, x:Double):Double = 0.0

    /**
     * Attempts to have this Wall collide with a Body.
     * @param body Body to collide with.
     * @return CollisionEvent if there was a collision, or null if none.
     */
    fun collide(thisRect: Rect2, body: Body): CollisionEvent?

    /**
     * Forces a body's bottom to be at the top of this Wall.
     */
    fun hook(thisRect: Rect2, body: Body)
}
