package engine.walls.platformer
import game.components.platformer.Body
import engine.core.Direction
import engine.core.Surface
import engine.core.SurfaceType
import engine.events.platformer.CollisionEvent
import engine.math.Rect2


/**
 * Represents a Wall in the shape of a Rect2.
 *
 * @param rect Rectangle determining the size and location of this Wall.
 * @param solidLeft Determines if solid in this direction.
 * @param solidRight Determines if solid in this direction.
 * @param solidTop Determines if solid in this direction.
 * @param solidBottom Determines if solid in this direction.
 * @param surface Surface to use on all edges.
 */
class RectWall
(
    val solidLeft:Boolean,
    val solidRight:Boolean,
    val solidBottom:Boolean,
    val solidTop:Boolean,
    override val surface: Surface = Surface(SurfaceType.DIRT, 0.7)
) : Wall

{
    override fun surfaceY(thisRect: Rect2, x:Double):Double = thisRect.top
    override fun hook(thisRect: Rect2, body: Body):Unit
    {
        body.forceBottom(thisRect.top)      // Places bottom of body on top.
        body.vel = body.vel.copy(y = 0.0)   // Sets vy to zero
        body.surface = surface              // Sets body's surface
    }
    override fun collide(thisRect: Rect2, body: Body): CollisionEvent?
    {
        // Extracts variables.
        val thatRect: Rect2 = body.rect           // Current Rectangle of that Wall
        val thatPrevRect: Rect2 = body.prevRect   // Previous Rectangle of that Wall

        // If in horizontal bounds...
        val thatLeft:Double = Math.min(thatRect.left, thatPrevRect.left)
        val thatRight:Double = Math.max(thatRect.right, thatPrevRect.right)
        if(thatRight >= thisRect.left && thatLeft <= thisRect.right)
        {
            // If top side is solid...
            if(solidTop)
            {
                // Calculate flags
                val impacted:Boolean = thatRect.bottom <= thisRect.top && thatPrevRect.bottom >= thisRect.top
                val exitSide:Direction? =
                    if(thatRect.right < thisRect.left) Direction.LEFT
                    else if(thatRect.left > thisRect.right) Direction.RIGHT
                    else null

                // Handles collision
                if (impacted)
                {
                    // Don't change positions in the event of an exit.
                    if(exitSide == null)
                    {
                        body.forceBottom(thisRect.top)      // Places bottom of body on top.
                        body.vel = body.vel.copy(y = 0.0)   // Sets vy to zero
                        body.surface = surface              // Sets body's surface
                    }

                    // Returns with collision event
                    return CollisionEvent(
                            isSurface = true,
                            impactSide = Direction.TOP,
                            exitSide = exitSide
                    )
                }
            }

            // Otherwise, if colliding with the bottom of this Wall...
            else if(solidBottom && thatRect.top >= thisRect.bottom && thatPrevRect.top <= thisRect.bottom)
            {
                body.forceTop(thisRect.bottom)  // Places top of body on bottom.
                body.vel = body.vel.copy(y=0.0) // Sets vy to zero

                // Returns with collision event
                return CollisionEvent(
                        isSurface = true,
                        impactSide = Direction.BOTTOM
                )
            }
        }

        // If in vertical bounds...
        if(thatRect.bottom <= thisRect.top && thatRect.top >= thisRect.bottom)
        {
            // If colliding with the left of this Wall...
            if(solidLeft && thatRect.right >= thisRect.left && thatPrevRect.right <= thisRect.left)
            {
                body.forceRight(thisRect.left)  // Places right of body on left.
                body.vel = body.vel.copy(x=0.0) // Sets vx to zero

                // Returns with collision event
                return CollisionEvent(impactSide = Direction.LEFT)
            }

            // Otherwise, if colliding with the rightof this Wall...
            if(solidRight && thatRect.left <= thisRect.right && thatPrevRect.left >= thisRect.right)
            {
                body.forceLeft(thisRect.right)  // Places left of body on right.
                body.vel = body.vel.copy(x=0.0) // Sets vx to zero

                // Returns with collision event
                return CollisionEvent(impactSide = Direction.RIGHT)
            }
        }

        // Default return value.
        return null
    }
}