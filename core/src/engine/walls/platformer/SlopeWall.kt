package engine.walls.platformer
import game.components.platformer.Body
import engine.events.platformer.CollisionEvent
import engine.math.*
import engine.core.*


/**
 * @author William Andrew Cahill
 * Slope representation of a Wall.
 *
 * @param p1 Relative coordinate (between 0 and 1)
 * @param p2 Relative coordinate (between 0 and 1)
 *
 * If p1.x < p2.x, then the slope is facing upwards and
 * enforces collision in that direction.
 *
 * If p1.x > p2.x, then the slope is facing downwards.
 *
 * If p1.x == p2.x, an RuntimeException will be thrown.
 */
data class SlopeWall(val p1: Vec2, val p2: Vec2, override val surface:Surface = Surface(SurfaceType.DIRT, 0.7)) : Wall
{
    /**
     * Initialization code
     */
    init
    {
        require(p1.x != p2.x){"Vertical slope not allowed"}
    }

    override fun hook(thisRect: Rect2, body: Body):Unit
    {
        val bodyX:Double = if(p2.y > p1.y) body.rect.right else body.rect.left
        val y:Double = surfaceY(thisRect, bodyX)
        body.forceBottom(y)
        val vel = body.vel
        body.vel = vel.copy(y = 0.0)
        body.surface = this.surface
    }

    /**
     * Gets surface y coordinate.
     */
    override fun surfaceY(thisRect: Rect2, x:Double):Double
    {
        // Keeps points on same side.
        val pone: Vec2 = if(p1.x < p2.x) p1 else p2
        val ptwo: Vec2 = if(p1.x < p2.x) p2 else p1

        // Calculates y value
        val value:Double =
            if(x < thisRect.left)
                thisRect.bottom + pone.y*thisRect.height
            else if(x > thisRect.right)
                thisRect.bottom + ptwo.y*thisRect.height
            else
            {
                // Transforms relative coordinates to map coordinates.
                val bl: Vec2 = thisRect.bottomLeft
                val pa: Vec2 = p1 * thisRect.size + bl
                val pb: Vec2 = p2 * thisRect.size + bl

                // Gets b and m values.
                val diff: Vec2 = pb - pa
                val m: Double = diff.y / diff.x
                val b: Double = pa.y - pa.x * m
                m*x+b
            }

        // Returns y value.
        return value
    }

    override fun collide(thisRect: Rect2, body: Body): CollisionEvent?
    {
        // Gets current and previous rects of body.
        val thatRect: Rect2 = body.rect
        val thatPrevRect: Rect2 = body.prevRect

        // If body is within horizontal bounds...
        val thatLeft:Double = Math.min(thatRect.left, thatPrevRect.left)
        val thatRight:Double = Math.max(thatRect.right, thatPrevRect.right)
        if(thatRight >= thisRect.left && thatLeft <= thisRect.right)
        {
            // If facing upwards...
            if (p2.x > p1.x)
            {
                // If this is facing left...
                if (p2.y > p1.y)
                {
                    // Gets current and previous positions of rect.
                    val pos: Vec2 = thatRect.bottomRight
                    val prevPos: Vec2 = thatPrevRect.bottomRight

                    // Gets current and previous intersection y value.
                    val y: Double = surfaceY(thisRect, pos.x)
                    val prevY: Double = surfaceY(thisRect, prevPos.x)

                    // Determines if a collision happened
                    val impacted:Boolean = (pos.y <= y && prevPos.y >= prevY)
                    val drifted:Boolean =
                        !impacted && body.prevOnGround &&
                        pos.y >= y && prevPos.y <= prevY && body.vel.y <= 0.0
                    val exitSide:Direction? =
                        if(thatRect.right < thisRect.left) Direction.LEFT
                        else if(thatRect.right > thisRect.right) Direction.RIGHT
                        else null

                    // Handles collision
                    if (impacted || drifted)
                    {
                        body.forceBottom(y)
                        val vel = body.vel
                        body.vel = vel.copy(y = 0.0)
                        body.surface = this.surface
                        return CollisionEvent(isSurface = true, impactSide = Direction.TOP, exitSide = exitSide)
                    }
                }

                // Otherwise, we're facing right
                else
                {
                    // Gets current and previous positions of rect.
                    val pos: Vec2 = thatRect.bottomLeft
                    val prevPos: Vec2 = thatPrevRect.bottomLeft

                    // Gets current and previous intersection y value.
                    val y: Double = surfaceY(thisRect, pos.x)
                    val prevY: Double = surfaceY(thisRect, prevPos.x)

                    // Determines if a collision happened
                    val impacted:Boolean = (pos.y <= y && prevPos.y >= prevY)
                    val drifted:Boolean =
                            !impacted && body.prevOnGround &&
                                    pos.y >= y && prevPos.y <= prevY && body.vel.y <= 0.0
                    val exitSide:Direction? =
                            if(thatRect.left < thisRect.left) Direction.LEFT
                            else if(thatRect.left > thisRect.right) Direction.RIGHT
                            else null

                    // Handles collision
                    if (impacted || drifted)
                    {
                        body.forceBottom(y)
                        val vel = body.vel
                        body.vel = vel.copy(y = 0.0)
                        body.surface = this.surface
                        return CollisionEvent(isSurface = true, impactSide = Direction.TOP, exitSide = exitSide)
                    }
                }
            }

            // Otherwise, assumes facing downwards
            else
            {
                // Gets on body to compare to
                var pos: Vec2
                var prevPos: Vec2
                if(p2.y > p1.y)
                {
                    pos = thatRect.topRight
                    prevPos = thatPrevRect.topRight
                }
                else
                {
                    pos = thatRect.topLeft
                    prevPos = thatPrevRect.topLeft
                }

                // Enforces collision
                val y: Double = surfaceY(thisRect, pos.x)
                val prevY: Double = surfaceY(thisRect, prevPos.x)
                if(pos.y >= y && prevPos.y <= prevY)
                {
                    body.forceTop(y)
                    if(body.vel.y > 0.0)
                    {
                        val vel: Vec2 = body.vel
                        val vx:Double =
                            if((p2.y > p1.y && vel.x > 0) || (p2.y < p1.y && vel.x < 0)) vel.x/4
                            else vel.x
                        val vy:Double = 0.0
                        body.vel = Vec2(vx, vy)
                    }
                }
            }
        }

        // Default return
        return null
    }
}