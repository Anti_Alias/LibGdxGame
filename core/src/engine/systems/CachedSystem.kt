package engine.systems
import com.badlogic.ashley.core.*


/**
 * Represents a System that caches Entities from the Engine it is stored in
 * when they become part of the family, and removed when they are not.
 *
 * @param family Family this CachedSystem should use when
 * evaluating relevant Entities.
 */
abstract class CachedSystem(val family:Family, initPriority:Int) : EntitySystem()
{
    // List of entites that belong to this System's family
    private var _cachedEntities:List<Entity> = listOf()

    // Listener used when Entities are added and removed.
    private val entityListener = object : EntityListener
    {
        // Handles Entity registration
        override fun entityAdded(e:Entity)
        {
            if(family.matches(e))
                _cachedEntities += e
        }

        // Handles Entity removal
        override fun entityRemoved(e:Entity)
        {
            if(family.matches(e))
                _cachedEntities -= e
        }
    }

    /**
     * List of Entities stor
     */
    val cachedEntities get() = _cachedEntities

    /**
     * Invoked when added to an Engine.
     */
    override fun addedToEngine(engine:Engine):Unit
    {
        // Adds all entities that are already stored to the cache.
        for(e in engine.entities)
        {
            if(family.matches(e))
                _cachedEntities += e
        }

        // Attaches listener to Engine which adds entities to the cache
        // when they get added to the Engine.
        engine.addEntityListener(family, entityListener)
    }

    /**
     * Invoked when removed from the Engine
     */
    override fun removedFromEngine(engine:Engine):Unit
    {
        // Clears cache.
        _cachedEntities = listOf()
    }

    // Initializes object
    init
    {
        this.priority = initPriority
    }
}