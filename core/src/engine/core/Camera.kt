package engine.core
import engine.math.Point2
import engine.math.Rect2
import engine.math.Vec2


/**
 * @author William Andrew Cahill
 *
 * Represents a Camera in 2D space used by the game engine
 * in order to render scenes by an offset, rotation and scale.
 */
class Camera(initSize: Vec2)
{
    /**
     * Center point of the Camera
     */
    var center: Vec2 = Vec2.ZERO

    /**
     * Point2 to target
     */
    var target: Point2? = null

    /**
     * Easing value
     */
    var ease: Double = 0.1
        get() = field
        set(value){require(value >= 0){"Easing value must be >= 0. Got $value"}}

    /**
     * Size of the Camera (width and height combined.)
     */
    var size: Vec2 = initSize
        get() = field
        set(value)
        {
            require(value.x >= 0 && value.y >= 0){"Size cannot be negative"}
        }

    /**
     * Represents the PlatformerArea in which the Camera must be in.
     * Has not affect if null.
     */
    var boundary: Rect2? = null

    /**
     * Rectangular space that this Camera takes up.
     */
    val area: Rect2 get() = Rect2(center.x-size.x/2, center.y-size.y/2, size.x, size.y)

    /**
     * Minimum x value of Camera.
     */
    val left:Double get() = center.x - size.x/2

    /**
     * Maximum x value of Camera.
     */
    val right:Double get() = center.x + size.x/2

    /**
     * Minimum y value of Camera.
     */
    val top:Double get() = center.y + size.y/2

    /**
     * Maximum y value of Camera.
     */
    val bottom:Double get() = center.y - size.y/2

    /**
     * Updates state of Camera to follow target if it is set.
     */
    internal fun update():Unit
    {
        // Gets dimensions
        val hwidth:Double = size.x/2
        val hheight:Double = size.y/2
        val boundary: Rect2? = this.boundary

        // Follows target if it exists.
        val target: Point2? = this.target
        if(target != null)
        {
            // Gets target coordinates
            var x:Double = target.x
            var y:Double = target.y

            // Restricts it for smoother camera movement in the event there is a boundary
            if(boundary != null)
            {
                if(x-hwidth < boundary.left) x = boundary.left + hwidth
                if(x+hwidth > boundary.right) x = boundary.right - hwidth
                if(y-hheight < boundary.bottom) y = boundary.bottom + hheight
                if(y+hheight > boundary.top) y = boundary.top - hheight
            }

            // Creates a new target using those coordinates,
            // then follows it
            val newTarget = Vec2(x ,y)
            val dist: Vec2 = newTarget - center
            val delta: Vec2 = dist * ease
            center += delta
        }

        // Forces self to stay in bounds if a boundary exists
        if(boundary != null)
        {
            if(left < boundary.left)
                center = center.copy(x = boundary.left + size.x/2f)
            if(right > boundary.right)
                center = center.copy(x = boundary.right - size.x/2f)
            if(bottom < boundary.bottom)
                center = center.copy(y = boundary.bottom + size.y/2f)
            if(top > boundary.top)
                center = center.copy(y = boundary.top - size.y/2f)
        }
    }
}