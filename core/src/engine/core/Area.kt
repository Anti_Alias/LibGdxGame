package engine.core
import com.badlogic.gdx.scenes.scene2d.Group

/**
 * Abstract representation for an area in a Game.
 * Areas are to be attached to a Game and represent the space in which logic happens.
 *
 * @author William Andrew Cahill
 */
interface Area : Disposable
{
    /**
     * Name of the Area
     */
    val name:String

    /**
     * Represents an Area's Camera
     */
    val camera:Camera

    /**
     * Causes Area's logic to be run.
     */
    fun run():Unit

    /**
     * Causes Area's graphics to be rendered.
     */
    fun render():Unit
}