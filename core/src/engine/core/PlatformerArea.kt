package engine.core
import com.badlogic.ashley.core.*
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.maps.*
import com.badlogic.gdx.maps.tiled.*
import com.badlogic.gdx.scenes.scene2d.*
import engine.graphics.*
import engine.math.*
import engine.walls.platformer.*
import game.components.platformer.Body
import game.generators.EntityGenerators
import game.systems.platformer.WallSystem


/**
 * @author William Andrew Cahill
 * Represents one PlatformerArea in a game.
 * Has 2D graphics, entities, sounds, etc.
 *
 * @param name Name of the PlatformerArea
 * @param engine Engine for processing logic.
 * @param resourceNames Resources that should be unloaded when this PlatformerArea gets deleted.
 */
class PlatformerArea(override val name:String) : Area
{
    // Private variables
    internal var _layers: List<Layer> = listOf()
    internal var _anchors:Map<String, Vec2> = mapOf()
    internal var _resources:Map<String, Int> = mapOf()
    internal var _pauseListeners:List<()->Unit> = listOf()
    internal var _unpauseListeners:List<()->Unit> = listOf()
    internal var _tickListeners:List<()->Unit> = listOf()
    internal var _prevIsPaused:Boolean = false

    /**
     * Ashley engine that powers Entity behavior in an PlatformerArea.
     */
    val engine: Engine = Engine()

    /**
     * Group that contains UI elements.
     * Useful for a pause menu.
     */
    val ui:Group = Group()


    /**
     * Represents viewable space.
     */
    override val camera:Camera = Camera(Vec2(Game.initWidth.toDouble(), Game.initHeight.toDouble()))

    /**
     * Paused flag.
     */
    var isPaused:Boolean = false

    /**
     * Toggles paused status
     */
    fun togglePause():Unit {isPaused = if(isPaused) false else true}

    /**
     * Stores a pause listener
     */
    fun addOnPause(listener: ()->Unit):Unit
    {
        _pauseListeners += listener
    }

    /**
     * Removes a pause listener
     */
    fun removeOnPause(listener: ()->Unit):Unit
    {
        _pauseListeners -= listener
    }

    /**
     * Stores a pause listener
     */
    fun addOnUnpause(listener: ()->Unit):Unit
    {
        _unpauseListeners += listener
    }

    /**
     * Removes a pause listener
     */
    fun removeOnUnPause(listener: ()->Unit):Unit
    {
        _unpauseListeners -= listener
    }

    /**
     * Adds a tick listener.
     * Invoked after game logic for a particular frame.
     */
    fun addOnTick(listener: ()->Unit):Unit
    {
        _tickListeners += listener
    }

    /**
     * Removes a tick listener
     */
    fun removeOnTick(listener: ()->Unit):Unit
    {
        _tickListeners -= listener
    }



    // Initializes code
    init
    {

        // Adds listener to Engine
        // that registers/de-registers its graphics when it gets and removes it.
        val bodyClass = Body::class.java
        val family:Family = Family.all(bodyClass).get()
        engine.addEntityListener(family, object : EntityListener
        {
            override fun entityAdded(entity:Entity)
            {
                // Adds Body's graphics to the object layer.
                val body: Body = entity.getComponent(bodyClass)
                val graphics: Actor = body.graphics
                val layerGraphics:Actor = _layers[body.desiredLayer].graphics
                when(layerGraphics)
                {
                    is Group -> layerGraphics.addActor(graphics)
                    else -> throw RuntimeException("Could not put Entity on layer ${body.desiredLayer} because it was a tile layer.")
                }
            }
            override fun entityRemoved(entity:Entity)
            {
                // Removes Body's graphics from its parent. (It is assumed that this is Entity layer's group)
                val body: Body = entity.getComponent(bodyClass)
                val graphics: Actor = body.graphics
                graphics.remove()
            }
        })
    }

    /**
     * Has this PlatformerArea track a resource by name.
     * This resource will be discarded from the Game's asset manager.
     * when the PlatformerArea is discarded.
     */
    fun track(resourceName:String):Unit
    {
        val count:Int = _resources.getOrDefault(resourceName, 0)
        _resources += Pair(resourceName, count+1)
    }


    /**
     * Mapping of anchor names to anchor values.
     * Each named anchor is just a point in 2D space.
     */
    val anchors:Map<String, Vec2> get() = _anchors

    /**
     * Forces internal camera to look at point on screen.
     */
    fun lookAt(vec: Vec2):Unit
    {
        camera.center = vec
    }


    /**
     * Optimizes Actor
     */
    private fun optimize(actor:Actor):Unit
    {
        // Gets camera area
        val camRect = camera.area

        // Optimizes actor
        when(actor)
        {
            is Optimizer -> actor.optimize(camRect)
            is Group ->
            {
                val children = actor.children
                for(child in children) optimize(child)
            }
        }
    }

    /**
     * Updates PlatformerArea logic. Runs internal Ashely engine.
     */
    override fun run():Unit
    {
        // Toggles pause if 'P' is pressed
        if(Gdx.input.isKeyJustPressed(Input.Keys.P))
            togglePause()

        // Moves camera
        camera.update()

        // Only runs game logic if not paused
        if(!isPaused)
            engine.update(Gdx.graphics.deltaTime)

        // Invokes tick listeners
        _tickListeners.forEach{it.invoke()}

        // Invokes callbacks if toggling between pauses.
        if(isPaused && !_prevIsPaused)
            _pauseListeners.forEach{it.invoke()}
        if(!isPaused && _prevIsPaused)
            _unpauseListeners.forEach{it.invoke()}

        // Syncs prev state
        _prevIsPaused = isPaused
    }

    /**
     * Renders PlatformerArea graphics. Utilizes Game's globally accessible SpriteBatch to render.
     */
    override fun render():Unit
    {
        // Optimizes layers
        _layers.forEach{optimize(it.graphics)}

        // Renders layers
        Game.batch.begin()
            for(layer: Layer in _layers)
            {
                val layerGraphics:Actor = layer.graphics
                val speed:Double = layer.speed
                val off: Vec2 = (camera.center * -speed) + (camera.size/2.0)
                layerGraphics.setPosition(off.x.toInt().toFloat(), off.y.toInt().toFloat())
                layerGraphics.draw(Game.batch, 1f)
            }

            // Renders UI layer
            ui.draw(Game.batch, 1.0f)
        Game.batch.end()
    }

    /**
     * Disposes of PlatformerArea resources
     */
    override fun dispose():Unit
    {
        // Disposes of all explicit resources.
        for((key:String, count:Int) in _resources)
        {
            for(i in (0 until count))
                Game.assets.unload(name)
        }

        // Disposes of all resources in Entities
        for(entity:Entity in engine.entities)
        {
            for(component: Component in entity.components)
            {
                when(component)
                {
                    is Disposable -> component.dispose()
                }
            }
        }
    }

    /**
     * Applies a TileLayer to this PlatformerArea.
     */
    internal fun applyTileLayer(tileLayer:TiledMapTileLayer)
    {
        // Creates layer from properties
        val props = tileLayer.properties
        val mapGraphics = MapGraphics(tileLayer)
        val layer:Layer = createLayer(props, mapGraphics)

        // Adds graphics
        _layers += layer
    }

    /**
     * Creates a Layer object using properties and graphics
     */
    private fun createLayer(props:MapProperties, graphics:Actor):Layer
    {
        val name = props.get("name", "", String::class.java)
        val r = props.get("r", "1.0", String::class.java).toFloat()
        val g = props.get("g", "1.0", String::class.java).toFloat()
        val b = props.get("b", "1.0", String::class.java).toFloat()
        val x = props.get("x", "1.0", String::class.java).toFloat()
        val y = props.get("y", "1.0", String::class.java).toFloat()
        val speed:Double = props.get("speed", "1.0", String::class.java).toDouble()
        graphics.setColor(r, g, b, 1f)
        graphics.setPosition(x, y)
        val group = Group()
        group.addActor(graphics)
        return Layer(name, graphics, speed)
    }

    /**
     * Applies a MapLayer to this PlatformerArea.
     */
    fun applyCollisionLayer(layerIndex:Int, layer: TiledMapTileLayer):Unit
    {
        // Gets wall size
        val width:Int = layer.width
        val height:Int = layer.height
        val wallWidth:Double = layer.tileWidth.toDouble()
        val wallHeight:Double = layer.tileHeight.toDouble()

        // Reads layer and stores Walls into an Array.
        val walls:Array<Wall?> = Array(width*height, {null})
        for(y in 0 until height)
        {
            for(x in 0 until width)
            {
                // Process this tile into a Wall.
                val cell: TiledMapTileLayer.Cell? = layer.getCell(x, y)
                val tile:TiledMapTile? = cell?.tile

                // If tile exists...
                if(tile != null)
                {
                    // Gets type of tile it is
                    val props:MapProperties = tile.properties
                    val type:String? = props["type"] as String?

                    if(type != null)
                    {
                        when(type)
                        {
                            "rect" ->
                            {
                                val solid:String? = props["solid"] as String?
                                if(solid != null)
                                {
                                    val solidLeft:Boolean = solid.indexOf('l') != -1
                                    val solidRight:Boolean = solid.indexOf('r') != -1
                                    val solidBottom:Boolean = solid.indexOf('d') != -1
                                    val solidTop:Boolean = solid.indexOf('u') != -1
                                    val rectWall = RectWall(solidLeft, solidRight, solidBottom, solidTop)
                                    walls[y*width+x] = rectWall
                                }
                                else throw RuntimeException("Expected solid property on rect on layer $layerIndex at coordinates $x, $y")
                            }
                            "slope"->
                            {
                                // Extracts point properties
                                val spx0:String? = props["px0"] as String?
                                val spy0:String? = props["py0"] as String?
                                val spx1:String? = props["px1"] as String?
                                val spy1:String? = props["py1"] as String?

                                // If they all exist...
                                if(spx0 != null && spx1 != null && spy0 != null && spy1 != null)
                                {
                                    // Convert them to doubles
                                    val px0:Double = spx0.toDouble()
                                    val py0:Double = spy0.toDouble()
                                    val px1:Double = spx1.toDouble()
                                    val py1:Double = spy1.toDouble()

                                    // Contains them in vectors
                                    val p0: Vec2 = Vec2(px0, py0)
                                    val p1: Vec2 = Vec2(px1, py1)

                                    // Add a slope using that information
                                    val slopeWall = SlopeWall(p0, p1)
                                    walls[y*width+x] = slopeWall
                                }
                                else throw RuntimeException("Missing a point property on slope on layer $layerIndex at coordinates $x, $y")
                            }
                            else -> throw RuntimeException("Collision tile of type '$type' invalid on layer $layerIndex at coordinates $x, $y")
                        }
                    }
                    else throw RuntimeException("Collision tile did not have a type on layer $layerIndex at coordinates $x, $y")
                }
            }
        }

        // Construct and add WallSystem to engine based on walls created
        engine.addSystem(WallSystem(walls, width, height, wallWidth, wallHeight))
    }

    /**
     * Applies an image layer ot the PlatformerArea.
     */
    private fun applyImageLayer(imageLayer:TiledMapImageLayer):Unit
    {
        // Gets properties
        val props:MapProperties = imageLayer.properties
        val name:String = props.get("name", "", String::class.java)
        val r:Float = props.get("r", "1", String::class.java).toFloat()
        val g:Float = props.get("g", "1", String::class.java).toFloat()
        val b:Float = props.get("b", "1", String::class.java).toFloat()

        // Gets image to draw
        val region: TextureRegion = imageLayer.textureRegion

        // Creates Layer graphics
        val sprite = Sprite(region)
        sprite.moveBy(-sprite.width/2f, -sprite.height/2f)
        sprite.setColor(r, g, b, 1f)
        val group = Group()
        group.addActor(sprite)

        // Adds layer to list of Layers
        _layers += Layer(name, group, 0.0)
    }

    /**
     * Applies a MapObject to this PlatformerArea.
     * @param layerIndex Assumed index layer of the MapObject
     * @param obj MapObject to process.
     */
    internal fun applyObject(layerIndex:Int, obj:MapObject)
    {
        // Gets possible type of object.
        val props: MapProperties = obj.properties
        val type:String? = props.get("type") as String?

        // Handles object based on its type.
        // Complains if there is no type.
        if(type != null) when(type)
        {
            "anchor" -> applyAnchor(layerIndex, obj)
            "spawner" -> applySpawner(layerIndex, obj)
            "boundary" -> applyBoundary(layerIndex, obj)
            else -> throw RuntimeException("Unexpected object type '$type' on layer $layerIndex")
        }
        else throw RuntimeException("Object missing type on layer $layerIndex")
    }

    /**
     * Handles anchor object frm TiledMap.
     */
    private fun applyAnchor(layerIndex:Int, obj:MapObject):Unit
    {
        // Gets properties
        val props:MapProperties = obj.properties

        // Expects name property to be present.
        val name:String? = props.get("name") as String?
        if(name != null)
        {
            // Adds named anchor.
            val x = props["x"] as Float
            val y = props["y"] as Float
            val width = props["width"] as Float
            val height = props["height"] as Float
            val cx = x + width/2
            val cy = y + height/2
            _anchors += Pair(name, Vec2(cx.toDouble(), cy.toDouble()))
        }

        // Otherwise, fail
        else throw RuntimeException("Anchor object missing name on layer $layerIndex")
    }


    /**
     * Handles spawner object from TiledMap.
     * This is used to spawn Entities into the PlatformerArea.
     */
    private fun applySpawner(layerIndex:Int, obj:MapObject):Unit
    {
        // Gets properties
        val props:MapProperties = obj.properties

        // Gets name of Spawner
        val name:String? = props["name"] as String?
        if(name != null)
        {
            // Gathers key/value pairs that start with a dash.
            // The dash on property names signifies that its a special property
            // be passed into EntityGenerator when invoked.
            val params:MutableMap<String, String> = mutableMapOf()
            for(propName:String in props.keys)
            {
                if(propName.startsWith("-"))
                {
                    val trimProp:String = propName.substring(1).trim()
                    val value:String = props[propName]!! as String
                    params.put(trimProp, value)
                }
            }

            // Generates an Entity using parameters.
            // Then, spawns it.
            val entity: Entity = EntityGenerators.generate(name, params)

            // If entity that was generated has a Body, set some nice default properties.
            val body: Body? = entity.getComponent(Body::class.java)
            if(body != null)
            {
                // Places body at spawn point
                val shapeX:Int = props.get("x", Int::class.java)
                val shapeY:Int = props.get("y", Int::class.java)
                val shapeWidth:Int = props.get("width", Int::class.java)
                val shapeHeight:Int = props.get("height", Int::class.java)
                val cx:Int = shapeX + shapeWidth/2
                val cy:Int = shapeY + shapeHeight/2
                body.rect = body.rect.copy(x=cx.toDouble()-body.rect.width/2, y=cy.toDouble()-body.rect.height/2)
                body.desiredLayer = layerIndex

                // If focus is set, have camera focus on this Entity
                val focus:String = props.get("focus", "false", String::class.java)
                if(focus == "true")
                {
                    camera.target = body.centerPoint
                }
            }

            // Adds Entity to the engine.
            engine.addEntity(entity)
        }
        else throw RuntimeException("Spawner did not have a name on layer $layerIndex")
    }

    /**
     * Applies a camera view to the PlatformerArea.
     */
    private fun applyBoundary(layerIndex:Int, obj:MapObject):Unit
    {
        // Gets area of boundary
        val props:MapProperties = obj.properties
        val x:Int = props.get("x", Int::class.java)
        val y:Int = props.get("y", Int::class.java)
        val width:Int = props.get("width", Int::class.java)
        val height:Int = props.get("height", Int::class.java)

        // Sets camera area
        camera.boundary = Rect2(x.toDouble(), y.toDouble(), width.toDouble(), height.toDouble())
    }

    /**
     * Loads an PlatformerArea from a TiledMap.
     * Does not include any Systems.
     *
     * @param name Name of the PlatformerArea to use.
     * @param tiledMap TiledMap to load content from.
     */
    fun applyTiledMap(name:String, tiledMap:TiledMap): PlatformerArea
    {
        // Creates empty PlatformerArea
        val area = PlatformerArea(name)

        // Handles layers.
        for((index:Int, mapLayer:MapLayer) in tiledMap.layers.withIndex())
        {
            // Gets layer type
            val layerType:String = mapLayer.properties.get("type", "tile", String::class.java)

            // Determines how to handle layer.
            when(mapLayer)
            {
                // Handles tile-based layers
                is TiledMapTileLayer ->
                {
                    if(layerType == "tile") applyTileLayer(mapLayer)
                    else if(layerType == "collision") applyCollisionLayer(index, mapLayer)
                    else throw RuntimeException("Invalid layer type $layerType on layer $index")
                }
                is TiledMapImageLayer ->
                {
                    applyImageLayer(mapLayer)
                }

                // Assumed object layer
                else ->
                {
                    // Adds graphical layer.
                    val props:MapProperties = mapLayer.properties
                    val group = Group()
                    val layer:Layer = createLayer(props, group)
                    _layers += layer

                    // Handles every object on that Tiled layer.
                    mapLayer.objects.forEach{applyObject(index, it)}
                }
            }
        }

        // Returns result PlatformerArea
        return area
    }


    /**
     * Helpful companion object to PlatformerArea class.
     */
    companion object
    {
        /**
         * Loads an PlatformerArea from a TiledMap.
         * Does not include any Systems
         *
         * @param name Name of TiledMap to load from.
         * This will be converted to the pathname "maps/$name.tmx"
         */
        fun loadFromTiledMap(name: String): PlatformerArea
        {
            // Gets TiledMap from file
            val pathname: String = "maps/$name.tmx"
            Game.assets.load(pathname, TiledMap::class.java)
            Game.assets.finishLoadingAsset(pathname)
            val map: TiledMap = Game.assets.get(pathname)

            // Creates empty PlatformerArea
            val area = PlatformerArea(name)

            // Applies map to empty PlatformerArea
            area.applyTiledMap(name, map)

            // Returns result
            return area
        }
    }
}