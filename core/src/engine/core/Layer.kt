package engine.core
import com.badlogic.gdx.scenes.scene2d.Actor


/**
 * @author William Andrew Cahill
 *
 * A grouping of an Actor with extra information regarding how it should be rendered.
 * Used in rendering TileMap layers.
 *
 * @param graphics Graphics to render.
 * @param speed Rate at which graphics speed at which graphics slide across the screen
 * when the camera moves. Default value is 1.0
 */
class Layer(val name:String, val graphics: Actor, var speed: Double = 1.0)