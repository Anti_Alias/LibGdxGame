package engine.core

/**
 *
 * Basic representation for the surface of some Wall.
 * Holds reference to surface type, friction, etc.
 *
 * @author William Andrew Cahill
 *
 * @param type SurfaceType of this Surface. Determine sound that plays
 * in certain scenarios.
 *
 * @param friction Frictional value of this surface.
 * Determines looseness of Entities riding on this surface.
 */
data class Surface(val type: SurfaceType, val friction:Double)