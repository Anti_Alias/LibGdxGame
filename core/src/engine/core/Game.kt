package engine.core
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Graphics
import com.badlogic.gdx.Input
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.maps.tiled.TmxMapLoader
import com.badlogic.gdx.Graphics.DisplayMode
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.graphics.glutils.FrameBuffer
import com.badlogic.gdx.math.Matrix4
import engine.input.Mouse
import engine.math.Vec2

/**
 * @author William Andrew Cahill
 * Singleton object representing the Game as a whole.
 * This should be controlled by the ApplicationListener class which LibGdx is dependent on.
 */
class Game
{
    companion object
    {
        private var _initWidth:Int = -1
        private var _initHeight:Int = -1
        private lateinit var _batch: SpriteBatch
        private lateinit var _assets: AssetManager
        private lateinit var _fbo:FrameBuffer
        private var _currentArea: Area? = null
        var scale2 = 1f


        /**
         * Possible current area in the game you are at.
         */
        val currentArea: Area? get() = _currentArea

        /**
         * SpriteBatch used for 2D graphics of Game.
         */
        val batch: SpriteBatch get() = _batch

        /**
         * Asset manager of the Game used for managing resources.
         */
        val assets: AssetManager get() = _assets

        /**
         * Display mode to use
         */
        val displayMode: Graphics.DisplayMode = findDisplayMode()

        /**
         * Initial width of the Game.
         */
        val initWidth:Int get() = _initWidth

        /**
         * Initial height of the Game.
         */
        val initHeight:Int get() = _initHeight


        /**
         * Initializes Game
         */
        internal fun init(initWidth:Int, initHeight:Int):Unit
        {
            // Stores initial size
            _initWidth = initWidth
            _initHeight = initHeight

            // Builds AssetManager
            _assets = AssetManager()                                // Creates initial AssetManager with basic loaders.
            _assets.setLoader(TiledMap::class.java, TmxMapLoader()) // Adds TmxMapLoader to manager.

            // Creaters frame buffer.
            _fbo = FrameBuffer(Pixmap.Format.RGBA8888, initWidth, initHeight, false)

            // Creates SpriteBatch for rendering 2D graphics
            _batch = SpriteBatch()
        }

        /**
         * Transitions to a given Area in the Game.
         * @param area Area to transition to.
         */
        fun transitionTo(area: Area?):Unit
        {
            val temp: Area? = _currentArea
            _currentArea = area
            temp?.dispose()
        }

        /**
         * Runs game logic for one frame.
         */
        internal fun run():Unit
        {
            // Run the current area if it exists
            currentArea?.run()
        }

        /**
         * Fullscreen flag.
         */
        val isFullscreen:Boolean get() = Gdx.graphics.isFullscreen


        /**
         * Forces Game to toggle fullscreen mode.
         */
        fun toggleFullscreen():Unit
        {
            if(!isFullscreen)
            {
                val mode:DisplayMode = findDisplayMode()
                Gdx.graphics.setFullscreenMode(mode)
            }
            else
            {
                Gdx.graphics.setWindowedMode(initWidth, initHeight)
            }
        }


        /**
         * Finds a "good" display mdoe to use.
         */
        private fun findDisplayMode(): Graphics.DisplayMode
        {
            // Gets all DisplayModes in order of resolution.
            val displayModes:Array<Graphics.DisplayMode> = Gdx.graphics.displayModes
            val sorter:Comparator<DisplayMode> = object : Comparator<DisplayMode>
            {
                override fun compare(d1:DisplayMode, d2:DisplayMode):Int
                {
                    val res1:Int = d1.width * d1.height
                    val res2:Int = d2.width * d2.height
                    return res1 - res2
                }
            }
            val newModes = displayModes
                .sortedWith(sorter)
                .filter{it.refreshRate > 55}

            // Uses largest display mode
            if(newModes.size == 0)
                throw RuntimeException("Could not find suitable resolution")
            return newModes[newModes.size-1]
        }

        /**
         * Renders current frame of Game.
         */
        internal fun render():Unit
        {
            // Checks for fullscreen toggle
            if(Gdx.input.isKeyPressed(Input.Keys.ALT_LEFT) && Gdx.input.isKeyJustPressed(Input.Keys.ENTER))
             toggleFullscreen()

            // Updates input
            val mx:Int = Gdx.input.getX()
            val my:Int = Gdx.graphics.height - Gdx.input.getY()
            Mouse._pos = Vec2(mx.toDouble(), my.toDouble())

            // Clears main screen
            Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1f)
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

            // Starts rendering to FBO
            _fbo.begin()

                // Clears FBO screen
                Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1f)
                Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

                // Renders current area if it is not null.
                currentArea?.render()

            // Finishes rendering to FBO
            _fbo.end()

            // Saves matrix state before transforming
            val matrix: Matrix4 = batch.transformMatrix
            val copy = Matrix4(matrix)

            // Transforms matrix for rendering offscreen texture
            val windowWidth:Int = Gdx.graphics.width
            val windowHeight:Int = Gdx.graphics.height
            val scaleX:Float = windowWidth / initWidth.toFloat()
            val scaleY:Float = windowHeight / initHeight.toFloat()
            val scale:Float = if(scaleX > scaleY) scaleX else scaleY
            matrix.translate(initWidth/2f, initHeight/2f, 0f)
            matrix.scale(scale, scale, 1f)
            matrix.scale(1/scaleX, 1/scaleY, 1f)
            matrix.translate(-initWidth/2f, -initHeight/2f, 0f)

            // Draws offscreen texture
            val offscreenTexture:Texture = _fbo.colorBufferTexture
            val region = TextureRegion(offscreenTexture)
            region.flip(false, true)
            batch.begin()
                batch.draw(region, 0f, 0f)
            batch.end()

            // Restores matrix state
            matrix.set(copy)
        }

        /**
         * Tears down Game
         */
        internal fun dispose():Unit
        {
            // Disposes of resources
            _fbo.dispose()
            _currentArea?.dispose()
        }
    }
}