package engine.core

/**
 * Represents a Directional value
 */
enum class Direction
{
    LEFT, RIGHT, TOP, BOTTOM
}
