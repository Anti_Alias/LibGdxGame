package engine.core
import com.badlogic.ashley.core.ComponentMapper
import game.components.platformer.Body
import game.components.platformer.Cloud
import game.components.platformer.Jumper

/**
 * @author William Andrew Cahill
 *
 * Globally accessible place to access Component mappers.
 */
object Mappers
{
    val bodyMapper: ComponentMapper<Body> = ComponentMapper.getFor(Body::class.java)
    val jumperMapper:ComponentMapper<Jumper> = ComponentMapper.getFor(Jumper::class.java)
    val cloudMapper:ComponentMapper<Cloud> = ComponentMapper.getFor(Cloud::class.java)

}
