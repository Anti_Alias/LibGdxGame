package engine.core

/**
 * @author William Andrew Cahill
 * Represents a type of surface.
 */
enum class SurfaceType
{
    GRASS, DIRT, ROCK, METAL, WATER, WOOD
}