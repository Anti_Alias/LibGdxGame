package engine.core

/**
 * Represents some object that hangs on to some resources. Resources are disposed when
 * dispose() method is invoked. Multiple invocations have no effect.
 *
 * @author William Andrew Cahill
 */
interface Disposable
{
    /**
     * Disposes of resources this object holds.
     */
    fun dispose():Unit
}