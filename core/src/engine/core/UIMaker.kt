package engine.core
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.Group
import engine.graphics.Sprite


/**
 * Singleton object that assists in the creation of
 * certain UI elements.
 */
object UIMaker
{
    /**
     * Constructs a pause menu actor.
     */
    fun makePauseMenu():Actor
    {
        // Creates button
        val sprite = Sprite.loadFromFile("ui/basicButton.9.png")

        // Makes container of UI elements.
        val group = Group()
        return group
    }
}
