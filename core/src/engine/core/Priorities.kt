package engine.core

/**
 * @author William Andrew Cahill
 *
 * Singleton object that stores named priority values to be used by
 * System objects. Useful for scheduling different behaviors.
 */
class Priorities
{
    companion object
    {
        /**
         * Timing for initializing Entities for game loop.
         * Usually reserved for capturing state before
         * changes happens.
         *
         * Happens before anything else.
         */
        val INIT:Int = 0

        /**
         * Timing when decisions are made. Useful for AI and controller input.
         */
        val DECIDE:Int = 1

        /**
         * Timing where decisions are executed. Some behaviors do not require
         * decisions. Movement based on velocity often goes here.
         */
        val EXECUTE:Int = 2

        /**
         * Timing where entity-to-entity collision occurs.
         * Happens after EXECUTE and often suppresses movement
         * that happens in that section.
         */
        val COLLISION_ENTITY:Int = 3

        /**
         * Timing where entity-to-map collision occurs.
         * Happens after COLLISION_ENTITY
         */
        val COLLISION_MAP:Int = 4

        /**
         * Synchronizes Component state.
         * This is often used for syncing scene-graph objects
         * with their current positions.
         */
        val SYNC:Int = 5
    }
}