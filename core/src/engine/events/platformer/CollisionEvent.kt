package engine.events.platformer
import engine.core.Direction


/**
 * @author William Andrew Cahill
 *
 * Represents the event in which a collision took place.
 * All boolean flags default to false.
 *
 * @param isSurface Determines if the object collided with the object's surface (top)
 * @param impactSide Side at which impact occurred, if any
 * @param exitSide Side at which exit occurred, if any
 * @param exitInner True if the exit (if there was one) was an inner exit.
 */
data class CollisionEvent(val isSurface:Boolean=false, val impactSide:Direction, val exitSide:Direction? = null, val exitInner:Boolean = false)
{
    val isImpact:Boolean get() = impactSide != null
    val isExit:Boolean get() = exitSide != null
}
