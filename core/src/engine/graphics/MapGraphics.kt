package engine.graphics
import engine.math.EMath
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.maps.tiled.TiledMapTile
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.Actor
import engine.math.Rect2


/**
 * @author William Andrew Cahill
 *
 * Represents a section of graphics of a tile layer from a TiledMap
 * @param tiledMap TiledMap that stores underlying graphics.
 * @param camera Dependency-injected camera.
 * @param layers Layers within the TiledMap to render.
 */
class MapGraphics(val tileLayer:TiledMapTileLayer) : Actor(), Optimizer
{

    /**
     * Minimum column of tiles rendered.
     */
    private var minX:Int = 0

    /**
     * Minimum row of tiles rendered.
     */
    private var minY:Int = 0

    /**
     * Maximum column of tiles rendered (inclusive).
     */
    private var maxX:Int = tileLayer.width-1

    /**
     * Maximum row of tiles rendered (inclusive).
     */
    private var maxY:Int = tileLayer.height-1


    /**
     * Draws tiles from underlying TiledMap
     */
    override fun draw(batch: Batch, parentAlpha: Float):Unit
    {
        // Get layer at that index
        val tileWidth:Float = tileLayer.tileWidth
        val tileHeight:Float = tileLayer.tileHeight

        // Draw tiles on that layer.
        for(y in minY..maxY)
        {
            for(x  in minX..maxX)
            {
                val cell: Cell? = tileLayer.getCell(x, y)
                if(cell != null)
                {
                    val tile: TiledMapTile? = cell.tile
                    if(tile != null)
                    {
                        val region: TextureRegion = tile.textureRegion
                        val drawX:Float = x*tileWidth
                        val drawY:Float = y*tileHeight
                        batch.draw(region, this.x + drawX, this.y + drawY)
                    }
                }
            }
        }
    }

    /**
     * Optimizes the Optimzier.
     */
    override fun optimize(rect: Rect2):Unit
    {
        // Gets screen gdx vectors
        val screenBottomLeft:Vector2 = rect.bottomLeft.toGdxVec()
        val screenTopRight:Vector2 = rect.topRight.toGdxVec()

        // Converts them to local
        val localBottomLeft = screenToLocalCoordinates(screenBottomLeft)
        val localTopRight = screenToLocalCoordinates(screenTopRight)


        // Converts local coordinates to tile coordinates
        val x0:Int = (localBottomLeft.x/tileLayer.tileWidth).toInt()
        val y0:Int = (localBottomLeft.y/tileLayer.tileHeight).toInt()
        val x1:Int = (localTopRight.x/tileLayer.tileWidth).toInt()
        val y1:Int = (localTopRight.y/tileLayer.tileHeight).toInt()

        // Optimizes between that range
        minX = EMath.clamp(x0, 0, x1)
        minY = EMath.clamp(y0, 0, y1)
        maxX = EMath.clamp(x1, minX, tileLayer.width-1)
        maxY = EMath.clamp(y1, minY, tileLayer.height-1)
    }
}