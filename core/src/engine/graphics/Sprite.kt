package engine.graphics
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.scenes.scene2d.Actor
import engine.core.Game


/**
 * @author William Andrew Cahill
 *
 * Represents an Image graphic stored in a Scene2D scene graph.
 */
class Sprite(val graphics: TextureRegion) : Actor()
{
    override fun getWidth():Float = graphics.regionWidth.toFloat()
    override fun getHeight():Float = graphics.regionHeight.toFloat()
    private var drawable:Boolean = true

    /**
     * Draws this Sprite
     */
    override fun draw(batch:Batch, parentAlpha:Float)
    {
        // Only draws if drawable
        if(drawable)
        {
            // Sets color
            val copyColor: Color = batch.getColor()
            batch.setColor(this.color)

            // Draws
            batch.draw(graphics, x, y)

            // Restores
            batch.setColor(copyColor)
        }
    }

    /**
     * Helps in loading Sprite
     */
    companion object
    {
        /**
         * Loads a Sprite from an image file.
         */
        fun loadFromFile(path:String):Sprite
        {
            // Loads Texture
            Game.assets.load(path, Texture::class.java)
            Game.assets.finishLoadingAsset(path)
            val tex:Texture = Game.assets.get(path)

            // Creates region
            val reg = TextureRegion(tex)
            return Sprite(reg)
        }
    }
}