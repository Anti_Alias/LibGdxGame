package engine.graphics
import com.badlogic.gdx.scenes.scene2d.Actor

/**
 * William Andrew Cahill
 *
 * Represents a Scene2D graphics implementation that draws nothing.
 */
class EmptyGraphics : Actor() {}