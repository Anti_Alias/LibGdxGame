package engine.graphics
import engine.math.Rect2


/**
 * Represents a Scene2D object
 * that can be optimized based on a viewing area.
 *
 * An Optimizer only draws the graphics that will appear on the screen.
 * It skips over parts that will not show up.
 */
interface Optimizer
{
    /**
     * X coordinate
     */
    fun getX():Float

    /**
     * Y coordinate
     */
    fun getY():Float

    /**
     * Width
     */
    fun getWidth():Float

    /**
     * Height
     */
    fun getHeight():Float

    /**
     * @return true if this Optimizer is on screen.
     */
    fun isOnScreen(rect: Rect2):Boolean =
        getX()+getWidth() > rect.left &&
        getX() < rect.right &&
        getY()+getHeight() > rect.bottom &&
        getY() < rect.top

    /**
     * Optimizes this Optimizer such that it will only
     * render the graphics that fit in a viewing area.
     *
     * @param rect PlatformerArea in which to draw.
     */
    fun optimize(rect: Rect2):Unit
    {
    }
}