package engine.input
import com.badlogic.gdx.Gdx
import engine.math.Vec2


/**
 * @author William Andrew Cahill
 *
 * Singleton object that represents the Mouse.
 */
object Mouse
{
    internal var _pos: Vec2 = Vec2.ZERO

    /**
     * Position of Mouse relative to bottom-left corner of screen.
     */
    val pos: Vec2
        get() = _pos

    /**
     * Position of Mouse relative to bottom-left corner of the screen relative to screen dimensions.
     * Coordinates are relative to screen size.
     */
    val screenPos: Vec2 get()
    {
        val temp = pos
        val x:Double = temp.x / Gdx.graphics.width
        val y:Double = temp.y / Gdx.graphics.height
        return Vec2(x, y)
    }

    /**
     * Position of Mouse relative to bottom-left corner of the screen relative to screen dimensions.
     * Coordinates are relative to screen size.
     */
    val centerScreenPos: Vec2 get()
    {
        val cx:Double = Gdx.graphics.width / 2.0
        val cy:Double = Gdx.graphics.height / 2.0
        val temp = pos - Vec2(cx, cy)
        val x:Double = temp.x / Gdx.graphics.width
        val y:Double = temp.y / Gdx.graphics.height
        return Vec2(x, y)
    }
}