package engine.math


/**
 * Created by William Andrew Cahill
 * Represents a rectangle in 2D space.
 *
 * @param x Left-most X coordinate.
 * @param y Right-most Y coordinate.
 * @param width Width of the Rect2. Must be positive.
 * @param height Height of the Rect2. Must be positive.
 */
data class Rect2(val x:Double, val y:Double, val width:Double, val height:Double)
{
    init
    {
        // Checks size
        require(width >= 0 && height >= 0){"Width/Height${width}X${height} is invalid."}
    }

    /**
     * Left-most X coordinate of Rect2.
     */
    val left:Double get() = x

    /**
     * Right-most X coordinate of Rect2.
     */
    val right:Double get() = x + width

    /**
     * Bottom-most Y coordinate of Rect2.
     */
    val bottom:Double get() = y

    /**
     * Top-most Y coordinate of Rect2.
     */
    val top:Double get() = y + height

    /**
     * Bottom-left most point.
     */
    val bottomLeft: Vec2 get() = Vec2(left, bottom)

    /**
     * Bottom-right most point.
     */
    val bottomRight: Vec2 get() = Vec2(right, bottom)

    /**
     * Top-left most point.
     */
    val topLeft: Vec2 get() = Vec2(left, top)

    /**
     * Top-right most point.
     */
    val topRight: Vec2 get() = Vec2(right, top)

    /**
     * Size of the Rect2 as a Vec2.
     */
    val size: Vec2 get() = Vec2(width, height)
}