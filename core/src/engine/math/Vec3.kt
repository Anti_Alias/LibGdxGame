package engine.math
import com.badlogic.gdx.math.Vector3

/**
 * @author William Andrew Cahill
 * Represents an immutable 3-Dimensional Vector object.
 */
data class Vec3(val x:Double, val y:Double, val z:Double)
{
    /**
     * Length of this Vec2 squared. Cheaper to compute
     * than len. Useful is just making comparison to another
     * Vec2's length.
     */
    val lenSquared get() = x*x + y*y + z*z

    /**
     * Length of Vec2. More expensiev than
     * lenSquared.
     */
    val len get() = Math.sqrt(lenSquared)

    /**
     * This plus another Vec2
     */
    operator fun plus(that: Vec3) = Vec3(x+that.x, y+that.y, z+that.z)

    /**
     * This minus another Vec2
     */
    operator fun minus(that: Vec3) = Vec3(x-that.x, y-that.y, z-that.z)

    /**
     * This times another Vec2
     */
    operator fun times(that: Vec3) = Vec3(x*that.x, y*that.y, z*that.z)

    /**
     * This divided by another Vec2
     */
    operator fun div(that: Vec3) = Vec3(x/that.x, y/that.y, z/that.z)

    /**
     * This times a scalar.
     */
    operator fun times(s:Double) = Vec3(x*s, y*s, z*s)

    /**
     * This divided by a scalar.
     */
    operator fun div(s:Double) = Vec3(x/s, y/s, z/s)

    /**
     * Converts self to a libgdx Vector2
     */
    fun toGdxVec(): Vector3 = Vector3(x.toFloat(), y.toFloat(), z.toFloat())

    /**
     * Drops z-axis
     */
    fun toVec2(): Vec2 = Vec2(x, y)

    /**
     * @return Unit vector of this Vector.
     */
    fun toUnit(): Vec3
    {
        val len:Double = this.len
        return Vec3(x/len, y/len, z/len)
    }

    /**
     * Helpful companion object.
     */
    companion object Constants
    {
        val ZERO = Vec3(0.0, 0.0, 0.0)
        val UP = Vec3(0.0, 1.0, 0.0)
        val DOWN = Vec3(0.0, -1.0, 0.0)
        val LEFT = Vec3(-1.0, 0.0, 0.0)
        val RIGHT = Vec3(1.0, 0.0, 0.0)
        val ONE = Vec3(1.0, 1.0, 0.0)
        val NEAR = Vec3(0.0, 0.0, 1.0)
        val FAR = Vec3(0.0, 0.0, -1.0)
    }
}