package engine.math


/**
 * Created by William Andrew Cahill
 * Represents a rectangle in 3D space.
 *
 * @param x Left-most bottom-most far-most X coordinate.
 * @param y Left-most bottom-most far-most Y coordinate.
 * @param z Left-most bottom-most far-most Z coordinate.
 * @param width Width of the Rect3. Must be positive.
 * @param height Height of the Rect3. Must be positive.
 * @param depth Depth of the Rect3. Must be positive
 */
data class Rect3(val x:Double, val y:Double, val z:Double, val width:Double, val height:Double, val depth:Double)
{
    init
    {
        // Checks size
        require(width >= 0 && height >= 0 && depth >= 0){"Width/Height/Depth ${width}X${height}X${depth} is invalid."}
    }

    /**
     * Left-most X coordinate of Rect3.
     */
    val left:Double get() = x

    /**
     * Right-most X coordinate of Rect3.
     */
    val right:Double get() = x + width

    /**
     * Bottom-most Y coordinate of Rect3.
     */
    val bottom:Double get() = y

    /**
     * Top-most Y coordinate of Rect3.
     */
    val top:Double get() = y + height

    /**
     * Far-most Z coordinate of Rect3
     */
    val far:Double get() = z

    /**
     * Near-most Z coordinate of Rect3
     */
    val near:Double get() = z + depth

    /**
     * Bottom-left most point.
     */
    val bottomFarLeft: Vec3 get() = Vec3(left, bottom, far)

    /**
     * Bottom-right most point.
     */
    val bottomFarRight: Vec3 get() = Vec3(right, bottom, far)

    /**
     * Top-left most point.
     */
    val topFarLeft: Vec3 get() = Vec3(left, top, far)

    /**
     * Top-right most point.
     */
    val topFarRight: Vec3 get() = Vec3(right, top, far)

    /**
     * Bottom-left most point.
     */
    val bottomNearLeft: Vec3 get() = Vec3(left, bottom, near)

    /**
     * Bottom-right most point.
     */
    val bottomNearRight: Vec3 get() = Vec3(right, bottom, near)

    /**
     * Top-left most point.
     */
    val topNearLeft: Vec3 get() = Vec3(left, top, near)

    /**
     * Top-right most point.
     */
    val topNearRight: Vec3 get() = Vec3(right, top, near)

    /**
     * Size of the Rect3 as a Vec2.
     */
    val size: Vec3 get() = Vec3(width, height, depth)
}