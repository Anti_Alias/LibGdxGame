package engine.math

/**
 * Represents a suite of functions and constants useful to the game engine.
 * @author William Andrew Cahill
 */
object EMath
{
    /**
     * Clamps a number.
     * @param n Number in question../test
     * @param a Small value.
     * @param b Large value.
     * @return the value of n clamped between and b.
     */
    fun clamp(n:Double, a:Double, b:Double):Double
    {
        if(n < a) return a
        else if(n > b) return b
        return n
    }

    /**
     * Clamps a number.
     * @param n Number in question.
     * @param a Small value.
     * @param b Large value.
     * @return the value of n clamped between and b.
     */
    fun clamp(n:Int, a:Int, b:Int):Int
    {
        if(n < a) return a
        else if(n > b) return b
        return n
    }
}