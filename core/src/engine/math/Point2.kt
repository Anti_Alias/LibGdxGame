package engine.math

/**
 * William Andrew Cahill
 *
 * Represents a point in 2D space.
 * Unlike Vec2, a Point2 can mutate. This is useful
 * when you need to follow a reference to some Vec2, for instance.
 */
interface Point2
{
    /**
     * Current x coordinate.
     */
    val x:Double

    /**
     * Current y coordinate.
     */
    val y:Double

    /**
     * Converts this mutable Point2 to an immutable Vec2.
     */
    fun toVec(): Vec2 = Vec2(x, y)
}