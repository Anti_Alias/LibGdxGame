package engine.math
import com.badlogic.gdx.math.Vector2

/**
 * @author William Andrew Cahill
 * Represents an immutable 2-Dimensional Vector object.
 */
data class Vec2(val x:Double, val y:Double)
{
    /**
     * Length of this Vec2 squared. Cheaper to compute
     * than len. Useful is just making comparison to another
     * Vec2's length.
     */
    val lenSquared get() = x*x + y*y

    /**
     * Length of Vec2. More expensiev than
     * lenSquared.
     */
    val len get() = Math.sqrt(lenSquared)

    /**
     * This plus another Vec2
     */
    operator fun plus(that: Vec2) = Vec2(x+that.x, y+that.y)

    /**
     * This minus another Vec2
     */
    operator fun minus(that: Vec2) = Vec2(x-that.x, y-that.y)

    /**
     * This times another Vec2
     */
    operator fun times(that: Vec2) = Vec2(x*that.x, y*that.y)

    /**
     * This divided by another Vec2
     */
    operator fun div(that: Vec2) = Vec2(x/that.x, y/that.y)

    /**
     * This times a scalar.
     */
    operator fun times(s:Double) = Vec2(x*s, y*s)

    /**
     * This divided by a scalar.
     */
    operator fun div(s:Double) = Vec2(x/s, y/s)

    /**
     * Converts self to a libgdx Vector2
     */
    fun toGdxVec():Vector2 = Vector2(x.toFloat(), y.toFloat())

    /**
     * @return Unit vector of this Vector.
     */
    fun toUnit(): Vec2
    {
        val len:Double = this.len
        return Vec2(x/len, y/len)
    }

    /**
     * Adds z value 0.0
     */
    fun toVec3(): Vec3 = Vec3(x, y, 0.0)

    /**
     * Helpful companion object.
     */
    companion object Constants
    {
        val ZERO = Vec2(0.0, 0.0)
        val UP = Vec2(0.0, 1.0)
        val DOWN = Vec2(0.0, -1.0)
        val LEFT = Vec2(-1.0, 0.0)
        val RIGHT = Vec2(1.0, 0.0)
        val ONE = Vec2(1.0, 1.0)
    }
}