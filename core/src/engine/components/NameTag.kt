package engine.components
import com.badlogic.ashley.core.Component

/**
 * @author William Andrew Cahill
 *
 * Represents the name of an Entity.
 * Can be used to identify one.
 *
 */
data class NameTag(val name:String) : Component
