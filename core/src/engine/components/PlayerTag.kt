package engine.components
import com.badlogic.ashley.core.Component


/**
 * @author William Andrew Cahill
 * Represents a Component that tags a particular Entity as a Player.
 */
class PlayerTag : Component