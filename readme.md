# Platformer Project
Thought I'd try my hand at creating a platformer when I have free time.
the project is written in Kotlin, of all languages.

## To run on Windows (gitbash/chocolatey)
    choco install jdk8
    git clone https://gitlab.com/Anti_Alias/LibGdxGame.git
    cd LibGdxGame
    ./gradlew.bat desktop:run

## To run on Debian Linux
    apt-get install jdk8
    git clone https://gitlab.com/Anti_Alias/LibGdxGame.git
    cd LibGdxGame
    ./gradlew desktop:run

Similar instructions apply to other Posix systems.